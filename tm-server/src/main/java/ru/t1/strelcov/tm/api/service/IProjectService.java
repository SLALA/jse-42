package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IBusinessService;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;

public interface IProjectService extends IBusinessService<ProjectDTO> {

    @NotNull
    ProjectDTO removeProjectWithTasksById(@Nullable String userId, @Nullable String projectId);

}
