package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.dto.model.AbstractBusinessEntityDTO;

public interface IBusinessRepository<E extends AbstractBusinessEntityDTO> extends IRepository<E> {

}
