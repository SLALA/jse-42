package ru.t1.strelcov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    public TaskListRequest(@Nullable final String token) {
        super(token);
    }

}
