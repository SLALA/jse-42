package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.request.TaskListByProjectIdRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskFindAllByProjectCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-find-all-by-project";
    }

    @NotNull
    @Override
    public String description() {
        return "Find tasks by project id.";
    }

    @Override
    public void execute() {
        @NotNull final ITaskEndpoint projectTaskEndpoint = serviceLocator.getTaskEndpoint();
        System.out.println("[FIND ALL PROJECT'S TASKS]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<TaskDTO> tasks = projectTaskEndpoint.listByProjectIdTask(new TaskListByProjectIdRequest(getToken(), projectId)).getList();
        int index = 1;
        for (final TaskDTO task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
